<?php
// Connecting to the database
require_once("includes/connect_db.php");
?>

<?php
if ($_GET['token'] == "") {
	echo "Invalid page";
}
else{
	// Retriveing the token 
	$token = $_GET['token']; //echo "$token";
?>
<html>
	<head>
		<title>Password reset</title>
	</head>
	<body>
		<?php include_once("includes/header.php"); ?>
		<p>Please enter your new password</p>
		<form method="POST" action="fgotpass_reset_db.php">
			<input type="password" name="password" required />
			<input type="submit" value="Reset password" />
		</form>
		<?php include_once("includes/footer.php"); ?>		
	</body>
</html>

<?php
}
?>