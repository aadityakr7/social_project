<?php include_once("includes/header.php"); ?>
<html>
	<head>
		<title>Forgot Password</title>
	</head>

	<body>
		<fieldset>
			<form method="POST" action="fgotpass_db.php">
				<h3>Find your account</h3><hr />
				<p>Please enter your username to get a password reset link on your email account.</p>
				<input type="text" name="uname" required />
				<input type="submit" value="Search" />
			</form>
		</fieldset>
	</body>
</html>
<?php include_once("includes/footer.php"); ?>