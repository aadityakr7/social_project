<?php
require_once("includes/connect_db.php");

// User function class
class userFunction{
	private $db_conn;

	function __construct($db){
		$this->db_conn = $db;
	}
	public function UserRegister($fname,$gender,$uname,$email,$hashed_password,$token,$verify_flag){
		// Writing the query
		$qry = "insert into users values('$fname','$gender','$uname','$email','$hashed_password','$token',$verify_flag)";
		// Executing the query
		$result = $this->db_conn->query($qry);
		if ($result) {
			header("Location: verify_user.php?token={$token}&email={$email}");
		}
		else{
			$errLogin = "Incorrect username/password combination";
			return $errLogin;
		}
	}

	public function UserLogin($uname,$hashed_password){
		// login code
		//print($uname.$password);
		$qry1 = "select * from users where uname='$uname' and password='$hashed_password'";
		$result1 = $this->db_conn->query($qry1);
		$row1 = $result1->fetch_assoc();
		//print_r($row1);
		if (count($row1)>0) {
			if ($row1['verify_flag'] == 1) {
				return true;
			}
		}
		else{
			return false;
		}
	}

	public function ForgotPassword($uname){
		// fgotpass code
		//Checking if the password entered is valid or not
		$qry2 = "select * from users";
		$res = $this->db_conn->query($qry2);
		while($row = $res->fetch_assoc())
		{
			if($row['uname'] == $uname)
			{
				$flag = 1;	//print_r($row);
				// finding the email of the user
				$email_var = $row['email'];
				break;
			}
			else
			{
				$flag = 0;
			}
		}

		if($flag == 1){
			// Generating url to send to the user
			$token2 = md5($uname);

			// Generating the forgot password link
			$fgot_url = "localhost/Social_Project/fgotpass_reset?token='$token2'";

			// Sending it to the user via email
			$mail_to=$email_var;
			$subject="Password recover";
			$message="Click this link to reset your password ".$fgot_url;
			$headers="From: website";
		
			$mail_status=mail($mail_to,$subject,$message,$headers);
		
			// Checking if it is sent or not
			if($mail_status){
				//return true;
				echo "Please check your email to reset your password";
			}
			else{
				//return false;
				echo "Some error in request";
			}

		}
		else{
			echo "Incorrect username";
		}

	}
}
?>