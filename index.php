<?php include_once("includes/header.php"); ?>
<html>
	<head>
		<title>Login Page</title>
	</head>

	<body>
		<fieldset>
			<legend>Login</legend>
			<form method="POST" action="login_db.php">
				<table cellspacing="5px">
					<tr>
						<td>Name: </td>
						<td><input type="text" name="uname" required="required" /></td>
					</tr>
					<tr>
						<td>Password: </td>
						<td><input type="password" name="password" required="required" /></td>
					</tr>
					<tr>
						<td align="right" colspan="2"><input type="submit" value="Login" /></td>
					</tr>
				</table>
			</form>
			<a href="fgotpass.php">Forgot password?</a>
			<a href="register.php">Sign up here</a>
		</fieldset>
	</body>
</html>
<?php include_once("includes/footer.php"); ?>